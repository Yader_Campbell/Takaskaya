package com.example.takaskaya.Chat;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.takaskaya.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class HolderMensajes extends RecyclerView.ViewHolder {

    private TextView nombreUser;
    private TextView mensaje;
    private TextView hora;
    private CircleImageView fotoPerfil;
    private ImageView fotoMensaje;

    public HolderMensajes(@NonNull View itemView) {
        super(itemView);

        nombreUser = (TextView)itemView.findViewById(R.id.nombreMensaje);
        mensaje = (TextView)itemView.findViewById(R.id.mesajeMensaje);
        hora = (TextView)itemView.findViewById(R.id.horamensaje);
        fotoPerfil = (CircleImageView)itemView.findViewById(R.id.fotoperfilmensaje);
        fotoMensaje = (ImageView)itemView.findViewById(R.id.mensajeFoto);

    }

    public TextView getNombreUser() {
        return nombreUser;
    }

    public void setNombreUser(TextView nombreUser) {
        this.nombreUser = nombreUser;
    }

    public TextView getMensaje() {
        return mensaje;
    }

    public void setMensaje(TextView mensaje) {
        this.mensaje = mensaje;
    }

    public TextView getHora() {
        return hora;
    }

    public void setHora(TextView hora) {
        this.hora = hora;
    }

    public CircleImageView getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(CircleImageView fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public ImageView getFotoMensaje() {
        return fotoMensaje;
    }

    public void setFotoMensaje(ImageView fotoMensaje) {
        this.fotoMensaje = fotoMensaje;
    }
}
