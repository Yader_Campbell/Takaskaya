package com.example.takaskaya.Chat.Entidades_Firebase;

public class Usuario {

    private String nombreUsuario;
    private String numeroCelular;
    private  String Usuarioedad;

    public Usuario() {
    }

    public String getUsuarioedad() {
        return Usuarioedad;
    }

    public void setUsuarioedad(String usuarioedad) {
        Usuarioedad = usuarioedad;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }
}