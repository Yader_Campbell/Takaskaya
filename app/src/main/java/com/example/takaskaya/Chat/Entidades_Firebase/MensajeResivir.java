package com.example.takaskaya.Chat.Entidades_Firebase;

public class MensajeResivir extends Mensaje {

    private Long hora;

    public MensajeResivir() {
    }

    public MensajeResivir(Long hora) {
        this.hora = hora;
    }

    public MensajeResivir(String mensaje, String urlFoto, String nombre, String fotoperfil, String type_mensaje, Long hora) {
        super(mensaje, urlFoto, nombre, fotoperfil, type_mensaje);
        this.hora = hora;
    }

    public Long getHora() {
        return hora;
    }

    public void setHora(Long hora) {
        this.hora = hora;
    }
}
