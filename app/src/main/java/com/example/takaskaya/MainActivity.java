package com.example.takaskaya;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private EditText mnombre ;
    private EditText medad;
    private EditText mnumero;
    private Spinner spinner;
    private Button mButtonSubirDatosFirebase;

    private int edad;
    private String nombre;
    private String code;
    private String telefono;

    //User Photo
    ImageView ImgUserPhoto;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;

    private FirebaseDatabase database;
    private DatabaseReference mRootReference;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner)findViewById(R.id.spinnerCountries);
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CountryData.CountryAreaCodes));

        database = FirebaseDatabase.getInstance();
        mRootReference = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        mButtonSubirDatosFirebase = findViewById(R.id.button_crear);

        mnombre = (EditText)findViewById(R.id.text_nombre);
        medad = (EditText)findViewById(R.id.text_edad);
        mnumero = (EditText)findViewById(R.id.text_numero);
        ImgUserPhoto = (ImageView)findViewById(R.id.regUserPhoto);

        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= 22){
                    checkAndRequestForPermission();
                }else{
                    openGallery();
                }
            }
        });


        mButtonSubirDatosFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre = mnombre.getText().toString();
                edad = Integer.parseInt(medad.getText().toString());
                telefono = mnumero.getText().toString().trim();

                code = CountryData.CountryAreaCodes[spinner.getSelectedItemPosition()];

                telefono = mnumero.getText().toString().trim();

                if(telefono.isEmpty()){
                    mnumero.setError("Ingrece su numero celular");
                    mnumero.requestFocus();
                    return;
                }

                cargarDatosFireBase(nombre, edad, telefono, code, mRootReference);

                String phoneNumber = code + telefono;
                Intent verificar = new Intent(MainActivity.this, VerifyPhone.class);
                verificar.putExtra("phonenumber", phoneNumber);
                startActivity(verificar);
            }
        });
    }

    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent,REQUESCODE);
    }

    private void checkAndRequestForPermission() {

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,Manifest.permission.READ_EXTERNAL_STORAGE)){
                Toast.makeText(MainActivity.this,"Aceptar los requerimientos",Toast.LENGTH_SHORT).show();
            }else{
                ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},PReqCode);
            }
        }
        else
            openGallery();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RESULT_OK && requestCode == REQUESCODE && data != null){
            pickedImgUri = data.getData();
            ImgUserPhoto.setImageURI(pickedImgUri);
        }
    }

    private void updateUserInfo(String name, Uri pickedImgUri,FirebaseUser currentUser){

        StorageReference mStorage = FirebaseStorage.getInstance().getReference().child("users_photos");
        final StorageReference imageFilePath = mStorage.child(pickedImgUri.getLastPathSegment());

        imageFilePath.putFile(pickedImgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                    }
                });

            }
        });

    }


    private void cargarDatosFireBase(String nombre, int edad, String telefono, String code, DatabaseReference mRootReference) {
        Map<String, Object> datosUsuario = new HashMap<>();
        datosUsuario.put("Nombre",nombre);
        datosUsuario.put("Edad",edad);
        datosUsuario.put("Telefono",code+telefono);

        mRootReference.child("Usuario").push().setValue(datosUsuario);
    }

}