package com.example.takaskaya.Instituciones;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.takaskaya.R;

public class area_nidiaWhite extends AppCompatActivity {

    private WebView wv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_nidia_white);

        wv1 = (WebView)findViewById(R.id.webNIDIA);

        String URL = "www.latice.org/kvin/es/amdmnwg1908es.html";

        wv1.setWebViewClient(new WebViewClient());
        wv1.loadUrl("https://"+URL);
    }
}
