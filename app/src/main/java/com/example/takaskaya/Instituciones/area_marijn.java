package com.example.takaskaya.Instituciones;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.example.takaskaya.R;

public class area_marijn extends AppCompatActivity {

    private WebView wv1;
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_marijn);

        wv1 = (WebView)findViewById(R.id.webMARIJN);
        text = (TextView) findViewById(R.id.Text_MARJIN);

        String URL = "www.fundacionmarijn.org.ni/programas.html";

        wv1.setWebViewClient(new WebViewClient());
        wv1.loadUrl("https://"+URL);
    }
}
