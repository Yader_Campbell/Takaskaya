package com.example.takaskaya.Instituciones;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.example.takaskaya.R;

public class area_ceimm extends AppCompatActivity {

    private WebView wv1;
    private ProgressBar progressBar;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_ceimm);

        wv1 = (WebView)findViewById(R.id.webCEIMM);
        progressBar = findViewById(R.id.progress_bar2);
        progressBar.setVisibility(View.VISIBLE);
        btn = (Button)findViewById(R.id.btnLLamar_ceimm);

        String URL = "www.uraccan.edu.ni/centro-de-estudios-e-informacion-de-la-mujer";
        wv1.setWebViewClient(new WebViewClient());

            wv1.setWebViewClient(new WebViewClient());
            wv1.loadUrl("https://" + URL);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent llamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+50584953929"));
                if(ActivityCompat.checkSelfPermission(area_ceimm.this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED)
                    return;
                startActivity(llamar);
            }
        });
    }

}
