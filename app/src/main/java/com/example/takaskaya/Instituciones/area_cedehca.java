package com.example.takaskaya.Instituciones;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.example.takaskaya.EncuestaMapsActivity;
import com.example.takaskaya.R;

public class area_cedehca extends AppCompatActivity {

    private WebView wv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_cedehca);

        wv1 = (WebView)findViewById(R.id.webCEDEHCA);

        String URL = "www.odha-ni.or/es/organismos/locales/cedehca";

        wv1.setWebViewClient(new WebViewClient());
        wv1.loadUrl("https://"+URL);
    }
}
