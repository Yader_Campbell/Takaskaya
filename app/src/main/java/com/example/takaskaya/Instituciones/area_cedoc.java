package com.example.takaskaya.Instituciones;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.takaskaya.R;

public class area_cedoc extends AppCompatActivity {

    private WebView wv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_cedoc);

        wv1 = (WebView)findViewById(R.id.webCEDOC);

        String URL = "www.policia.gob.ni/cedoc/sector/dcmn/fich.html";

        wv1.setWebViewClient(new WebViewClient());
        wv1.loadUrl("https://"+URL);
    }
}
