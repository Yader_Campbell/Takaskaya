package com.example.takaskaya.ui.informacion;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.takaskaya.Informacion.InfQueEs;
import com.example.takaskaya.R;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.fragment_informacion, container, false);


        Button buttoninfQ = (Button) root.findViewById(R.id.button_queEs);
        buttoninfQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent informacion = new Intent(getActivity(), InfQueEs.class);
                informacion.putExtra("inf","some data");
                startActivity(informacion);
            }
        });

        final TextView textView = root.findViewById(R.id.text_gallery);
        galleryViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}