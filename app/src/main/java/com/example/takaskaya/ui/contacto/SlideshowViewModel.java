package com.example.takaskaya.ui.contacto;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SlideshowViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SlideshowViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("En caso de emergencia\ncontactar a las instituciones aliadas");
    }

    public LiveData<String> getText() {
        return mText;
    }
}