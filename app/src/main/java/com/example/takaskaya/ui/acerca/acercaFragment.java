package com.example.takaskaya.ui.acerca;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.takaskaya.R;
import com.example.takaskaya.Activity_acerca_app;
import com.example.takaskaya.activity_acerca_credito;

public class acercaFragment extends Fragment {

    private acercaViewModel acercaViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        acercaViewModel =
                ViewModelProviders.of(this).get(acercaViewModel.class);
        View root = inflater.inflate(R.layout.fragment_acerca, container, false);

        Button btnacer = (Button) root.findViewById(R.id.button_credito);
        btnacer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent credito = new Intent(getActivity(), activity_acerca_credito.class);
                startActivity(credito);
            }
        });

        Button btnacerApp = (Button) root.findViewById(R.id.button_acerca_app);
        btnacerApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent acerca_app = new Intent(getActivity(),Activity_acerca_app.class);
                startActivity(acerca_app);
            }
        });

        final TextView textView = root.findViewById(R.id.text_acerca);
        acercaViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);

            }
        });
        return root;
    }

}