package com.example.takaskaya.ui.acerca;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class acercaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public acercaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Acerca de la App");
    }

    public LiveData<String> getText() {
        return mText;
    }
}