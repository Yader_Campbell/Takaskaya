package com.example.takaskaya.ui.contacto;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.takaskaya.R;
import com.example.takaskaya.Instituciones.area_marijn;
import com.example.takaskaya.Instituciones.area_ceimm;
import com.example.takaskaya.Instituciones.area_nidiaWhite;
import com.example.takaskaya.Instituciones.area_fadcanic;
import com.example.takaskaya.Instituciones.area_cedehca;
import com.example.takaskaya.Instituciones.area_cedoc;

public class SlideshowFragment extends Fragment {

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.fragment_contacto, container, false);

        ImageButton ceimm = (ImageButton) root.findViewById(R.id.Button_ceimm);
        ceimm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ceimm = new Intent(getActivity(), area_ceimm.class);
                startActivity(ceimm);
            }
        });

        ImageButton marijn = (ImageButton) root.findViewById(R.id.Button_marinj);
        marijn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent marijn= new Intent(getActivity(), area_marijn.class);
                startActivity(marijn);
            }
        });

        ImageButton nidiaWhite = (ImageButton) root.findViewById(R.id.Button_nidiaWhite);
        nidiaWhite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nidia= new Intent(getActivity(), area_nidiaWhite.class);
                startActivity(nidia);
            }
        });

        ImageButton fadcanic = (ImageButton) root.findViewById(R.id.Button_fatcanic);
        fadcanic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fadcanic= new Intent(getActivity(), area_fadcanic.class);
                startActivity(fadcanic);
            }
        });

        ImageButton cedehca = (ImageButton) root.findViewById(R.id.Button_cedehca);
        cedehca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cedehca= new Intent(getActivity(), area_cedehca.class);
                startActivity(cedehca);
            }
        });

        ImageButton cedoc = (ImageButton) root.findViewById(R.id.Button_cedoc);
        cedoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cedoc= new Intent(getActivity(), area_cedoc.class);
                startActivity(cedoc);
            }
        });

        final TextView textView = root.findViewById(R.id.text_slideshow);
        slideshowViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}