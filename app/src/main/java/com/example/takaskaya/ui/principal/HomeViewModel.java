package com.example.takaskaya.ui.principal;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText,dText;

    public HomeViewModel() {
        mText = new MutableLiveData<>();
        dText = new MutableLiveData<>();
        mText.setValue("Bienvenido a TAKASKAYA");
        dText.setValue("\"No estás sola, denuncia, ¡somos libres!\"");
    }

    public LiveData<String> getText() {
        return mText;
    }
}