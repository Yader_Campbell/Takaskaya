package com.example.takaskaya;

public class CountryData {

    public static final String[] CountryNames = {"Belize", "Guatemala", "Nicaragua", "Honduras",
            "Panama", "Costa Rica", "El Salvador"
    };

    public static final String[] CountryAreaCodes = { "+501", "+502", "+505", "+504",
            "+507", "+506", "+503"
    };
}
